(in-package :proc)
   
(define-foreign-library libproc
    (:unix (:or "libproc-3.2.7.so" "libproc.so"))
  (t (:default "libproc-dev")))

(use-foreign-library libproc)

(defmacro with-sysctl (name vartype &body body)
  (let ((len (gensym))
	(p (car vartype))
	(type (cadr vartype)))
    `(with-foreign-objects
	 ((,p ,type)
	  (,len 'size-t))
       (setf (mem-ref ,len 'size-t) (foreign-type-size ,type))
       (foreign-funcall
	"sysctlbyname"
	:string ,name
	:pointer ,p
	:pointer ,len
	:pointer (null-pointer)
	size-t 0
	:int)
       (progn ,@body))))
    
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun get-nr-cpus ()
    (with-sysctl "hw.ncpu"
	(p :int)
      (convert-from-foreign (mem-ref p :int) :int))))

(defun boottime ()
  (with-sysctl "kern.boottime"
      (p 'timeval)
    (foreign-slot-value p 'timeval 'sec)))

(defun uptime ()
  (- (unix-time-now) (boottime)))
	  
(defconstant +nr-of-cpus+ (get-nr-cpus))


(defun mappids (function)
  (with-foreign-objects
      ((len 'size-t))
    (foreign-funcall
     "sysctlbyname"
     :string "kern.proc.all"
     :pointer (null-pointer)
     :pointer len
     :pointer (null-pointer)
     size-t 0
     :int)
    (let ((size (/ (convert-from-foreign (mem-ref len 'size-t) 'size-t)
		   (foreign-type-size 'proc))))
      (with-foreign-objects
	  ((p 'proc size)
	   (sizet 'size-t))
	(setf (mem-ref sizet 'size-t) (convert-to-foreign (* size (foreign-type-size 'proc))  'size-t))
	(foreign-funcall
	 "sysctlbyname"
	 :string "kern.proc.all"
	 :pointer p
	 :pointer sizet
	 :pointer (null-pointer)
	 size-t 0
	     :int)
	(loop for i from 0 below (1- (/ (convert-from-foreign (mem-ref sizet 'size-t) 'size-t)
					(foreign-type-size 'proc))) ;;quick fix, we otherwise get a pid 0 as well :x
		 collect (funcall function (mem-aref p 'proc i)))))))

(defun get-pids-info (&rest slots)
  (if (null slots)
      nil
      (mappids
       (lambda (pid)
	 (loop for slot in slots
	       collect (foreign-slot-value pid 'proc slot))))))

(defun get-pids ()
  (mapcar #'car (get-pids-info 'tid)))


(defun funpid (pid fun)
  (with-foreign-objects
      ((mib :int 4)
       (mlen 'size-t)
       (len 'size-t)
       (kp 'proc))
    (setf (mem-ref mlen 'size-t) 4)
    (foreign-funcall
     "sysctlnametomib"
     :string "kern.proc.pid"
     :pointer mib
     :pointer mlen
     :int)
    (setf (mem-aref mib :int 3) pid)
    (setf (mem-ref len 'size-t) (foreign-type-size 'proc))
    (foreign-funcall
     "sysctl"
     :pointer mib
     size-t 4
     :pointer kp
     :pointer len
     :pointer (null-pointer)
     size-t 0
     :int)
    (funcall fun kp)))

(defun get-pid-info (pid &rest slots)
  (if (null slots)
      nil
      (funpid
       pid
       (lambda (p)
	 (loop for s in slots
	       collect (foreign-slot-value p 'proc s))))))

(defun get-priority (pid)
  (funpid
   pid
   (lambda (p)
     (foreign-slot-value 
      (foreign-slot-value p 'proc 'priority)
      'priority
      'priority))))
