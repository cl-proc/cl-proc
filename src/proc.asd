(cl:eval-when (:load-toplevel :execute)
 (asdf:operate 'asdf:load-op 'cffi-grovel))

(asdf:defsystem :proc
 :name "Proc"
 :version "0.0.2"
 :maintainer "Reinout Stevens"
 :author "Reinout Stevens <resteven@vub.ac.be>"
 :description "Bindings for libproc-dev"
 :depends-on (:cffi :local-time)
 :serial T
 :components
 ((:file "packages")
  (cffi-grovel:grovel-file #+linux "grovel-linux" #+bsd "grovel-bsd")
  (:file "procbase")
  (:file #+linux "proc-linux" #+bsd "proc-bsd")))
