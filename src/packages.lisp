(defpackage :proc
  (:use :common-lisp :cffi :local-time)
  (:export
   #:kill
   #:signal-name-to-number
   #:signal-number-to-name
   #:+nr-of-cpus+
   #:get-pid-info
   #:get-pids
   #:get-pids-info
   #:get-start-time
   #:uptime
   #:boottime
   #:get-elapsed-time))
