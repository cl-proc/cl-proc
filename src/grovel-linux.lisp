(in-package :proc)
(include "sys/sysinfo.h" "sys/types.h" "signal.h" "proc/sysinfo.h" "proc/readproc.h" "asm/param.h")

(ctype pid "pid_t")
(constant (+hz+ "HZ"))
(cstruct sysinfo "struct sysinfo"
	 (uptime "uptime" :type :long)
	 (loads "loads" :type :unsigned-long :count 3)
	 (freeram "freeram" :type :unsigned-long)
	 (sharedram "sharedram" :type :unsigned-long) ; /* Amount of shared memory */
	 (bufferram "bufferram" :type :unsigned-long) ; /* Memory used by buffers */
	 (totalswap "totalswap" :type :unsigned-long) ; /* Total swap space size */
	 (freeswap "freeswap" :type :unsigned-long) ;  /* swap space still available */
	 (procs "procs" :type :unsigned-short) ;    /* Number of current processes */
	 (totalhigh "totalhigh" :type :unsigned-long) ; /* Total high memory size */
	 (freehigh "freehigh" :type :unsigned-long) ;  /* Available high memory size */
	 (mem_unit "mem_unit" :type :unsigned-int)) ;   /* Memory unit size in bytes */

(ctype klong "KLONG")

(cstruct proc "struct proc_t"
	 (tid "tid" :type :int)
	 (ppid "ppid" :type :int)
	 (state "state" :type :char)
	 (start-time "start_time" :type :unsigned-long-long)
	 (priority "priority" :type :long)
	 (nice "nice" :type :long))
