;;package with non OS specific code
(in-package :proc)

(defun unix-time-now ()
  "Gives the current time in unixtime"
  (local-time:timestamp-to-unix (local-time:now)))
