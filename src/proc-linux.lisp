(in-package :proc)
   
(define-foreign-library libproc
    (:unix (:or "libproc-3.2.8.so" "libproc-3.2.7.so" "libproc.so"))
  (t (:default "libproc-dev")))
   
(use-foreign-library libproc)


;;sig.h
(defcfun "signal_name_to_number" :int
  "Converts a signal name to the corresponding number, see man kill"
  (name :string))

(defcfun "signal_number_to_name" :string
  "Converts a signal number to the corresponding name, see man kill"
  (number :int))

;;sysinfo.h
(defcvar ("smp_num_cpus" +nr-of-cpus+ :read-only t) :long
  "The number of cpus on the machine")

;;no idea what the difference is between both :x
(defconstant +no-stat+ 0)
(defconstant +do-stat+ 1)

(defcfun "readproctab" :pointer
  (flag :int))

(defcfun "freeproc" :void
  (proc :pointer))

(defcfun "kill" :int
  "kills a process as defined by man 2 kill"
  (pid pid)
  (sig :int))

(defun map-pids (function)
  "Maps a function over all the process ids
Function is called with 1 argument: the foreign proc-structure"
  (let* ((procs (readproctab +do-stat+))
	 (res
	  (loop for i = 0 then (1+ i)
		for p = (mem-aref procs :pointer i)
		while (not (null-pointer-p p))
		collect (funcall function p))))
    (foreign-free procs)
    res))


(defun fun-pid (pid fun)
  "Calls a function on a foreign proc-structure"
  (with-foreign-object (pt 'proc)
    (foreign-funcall "get_proc_stats" pid pid :pointer pt :pointer)
    (funcall fun pt)))


(defun convert-return-value (slot value)
  "Converts the return value for compatibility"
  (case slot
    (start-time (jiffie-to-time value))
    (otherwise value)))

(defun convert-slot (slot)
  "Converts the accessor for compatibility"
  (case slot
    (otherwise slot)))


(defun get-proc-slot (proc slot)
  "Access a foreign proc structure in an OS independent way"
  (convert-return-value
   slot
   (foreign-slot-value proc 'proc (convert-slot slot))))


(defun get-pid-info (pid &rest slots)
  "Gets all the information specified by slots."
  (if (null slots)
      nil
      (fun-pid
       pid
       (lambda (p)
	 (loop for s in slots
	    collect
	      (get-proc-slot p s))))))


(defun get-pids ()
  (map-pids
   (lambda (p)
     (get-proc-slot p 'tid))))


(defun get-pids-info (&rest slots)
  (if (null slots)
      nil
      (map-pids
       (lambda (p)
	 (loop for s in slots
	       collect (get-proc-slot p s))))))

	       

(defun jiffie-to-time (jiffies)
  "Some times get returned in jiffies, like start-time
and need to be converted to unixtime. Read man 5 proc"
  ;;as we don't get that much precision back it has no use to provide it to the user
  (values (truncate (/ jiffies +hz+))))


(defun get-start-time (pid)
  "gives the start time of pid after boot
behaviour is undefined for a non existing pid"
  (car (get-pid-info pid 'start-time)))
  

(defun get-elapsed-time (pid)
  "Returns the nr of seconds a certain process is running"
  (- (unix-time-now) (+ (boottime) (get-start-time pid))))


(defun uptime ()
  "Gives the uptime of the machine in unixtime"
  (with-foreign-object (pt 'sysinfo)
    (foreign-funcall "sysinfo" :pointer pt :int)
    (foreign-slot-value pt 'sysinfo 'uptime)))

(defun boottime ()
  "Unixtime of the moment you've booted your machine"
  (- (unix-time-now) (uptime)))
