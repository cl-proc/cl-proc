(in-package :proc)
(include "sys/types.h" "sys/sysctl.h" "sys/time.h" "sys/priority.h" "sys/user.h")

(ctype pid "pid_t")
(ctype size-t "size_t")
(ctype time-t "time_t")

(cstruct timeval "struct timeval"
	 (sec "tv_sec" :type time-t))

(cstruct priority "struct priority"
	 (priority "pri_level" :type :uchar))

(cstruct proc "struct kinfo_proc"
	 (tid "ki_pid" :type pid)
	 (ppid "ki_ppid" :type pid)
	 (state "ki_stat" :type :char)
	 (nice "ki_nice" :type :char)
	 (start-time "ki_start" :type timeval)
	 (priority "ki_pri" :type priority))
